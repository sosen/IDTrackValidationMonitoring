2016-12-09 Shaun Roe
	* Introduce complete TEfficiency implementation
	* tag as TrkValHistUtils-00-01-20

2016-11-28 Shaun Roe
	* Fix coverity defects:
108329 06/04/2016 (Medium) Uninitialized pointer field :/Tracking/TrkValidation/TrkValHistUtils/Root/TruthTrkExtrapolationPlots.cxx in function "ExtrLayerPlots"
108330 06/04/2016 (Medium) Uninitialized pointer field :/Tracking/TrkValidation/TrkValHistUtils/Root/TruthTrkExtrapolationPlots.cxx in function "ExtrRegionPlots"
	* tag as TrkValHistUtils-00-01-19

2016-11-03 Noemi Calace
	* Adding TProfile "S" options (OBO Nora Pettersson)
	* Tag as TrkValHistUtils-00-01-17 and TrkValHistUtils-00-01-18

2016-10-27 Noemi Calace
	* Adding TProfile2D options
	* Tag as TrkValHistUtils-00-01-16

2016-09-06 Shaun Roe
	* Fix bug in filling SCT outliers (IDHitPlots.cxx)
	* Tag as TrkValHistUtils-00-01-15 

2016-07-24 Shaun Roe
	* Add TEfficiency booking
	* Tag as TrkValHistUtils-00-01-14

2016-07-15 Shaun Roe
	* turn off adding histos to default object list, to suppress duplicate histogram warnings
	* Tagging TrkValHistUtils-00-01-13 
2016-03-17 Ioannis Nomidis <ioannis.nomidis@cern.ch>
	* Changed TH?D to TH?F to reduce memory footprint
	* Added class for muon truth extrapolation studies (disabled to maintain compatibility)
	* Tagging TrkValHistUtils-00-01-12

2015-06-22  Massimiliano Bellomo  <massimiliano.bellomo@cern.ch>
	* reverted back change from TrkValHistUtils-00-01-08 to be cache compatible
	* Tagging TrkValHistUtils-00-01-11

2015-06-08 <Ioannis.Nomidis@cern.ch>
	* Changes for athena/rootcore/athAnalysisBase compatibility
	* Tagging TrkValHistUtils-00-01-10

2015-06-02 Maximiliano Sioli <msioli@cern.ch>
	* Implementation of variable size binning for 3D histograms

2015-05-18 Shaun Roe
	* Changes to ensure that MuonSpectrometer/MuonValidation/MuonHistogramming/MuonHistUtils compiles against this
	* Tag as TrkValHistUtils-00-01-09

2015-05-12  Massimiliano Bellomo  <massimiliano.bellomo@cern.ch>
	* reverted back last change and added method to switch between
	directory substructure and prepending directory name to hist name
	* Tagging as TrkValHistUtils-00-01-08

2015-05-05 Shaun Roe
	* make truthInfo plot filling conditional on the info existing, to avoid crashes
	* Tagging as TrkValHistUtils-00-01-07

2015-04-22 Graham Cree <gcree@cern.ch>
	* Support for AthAnalysisBase
	* Tagging as TrkValHistUtils-00-01-06

2015-04-22 Graham Cree <gcree@cern.ch>
	* Dual-use support Athena/RootCore
	* Tagging as TrkValHistUtils-00-01-05

2015-02-26 Ioannis
	* TrkValHistUtils-00-01-03

2015-02-23 Heberth Torres <htorres@cern.ch>
	* Added method to store a TTree in PlotBase
	* Tagging as TrkValHistUtils-00-01-02

2015-02-21 Daniel Mori <dmori@cern.ch>
	* fixed pull plots
	* TrkValHistUtils-00-01-01

2015-02-19 Ioannis Nomidis
	* Changes to constructors to control hist name 
	* TrkValHistUtils-00-01-00

2015-02-18 Daniel Mori
	* Fixed binning for 2D PullPlots
	* TrkValHistUtils-00-00-27

2015-02-16 Shaun Roe, Ioannis Nomidis
	* Add BookTProfile2D method in plot base
	* Added varible binning option for Book1D and Book2D
	* TrkValHistUtils-00-00-26
	
2015-02-03 <Ioannis.Nomidis@cern.ch>
	* Changes in PlotBase: Overloaded Book2D for hists with variable x-axis binnning
	* Changes in DefParamPullPlots: fix call to Book2D
	* TrkValHistUtils-00-00-25

2015-02-03 <Ioannis.Nomidis@cern.ch>
	* Added histograms for muon truth extrapolated 
	* TrkValHistUtils-00-00-24

2015-01-25 Daniel Mori <dmori@cern.ch>
	* Added 2d hists of pull plots vs pt
	* TrkValHistUtils-00-00-23

2015-01-20 <Ioannis.Nomidis@cern.ch>
	* Changes in RecoInfoPlots: Added hist of p(chi2), changed binning in hist of ndf
	* Changes in PlotBase: Change in BookTProfile(), option to set y-value limits
	* TrkValHistUtils-00-00-22

2015-01-06 Daniel Mori <dmori@cern.ch>
	* modified DefParamPullPlots
	* TrkValHistUtils-00-00-21

2014-12-08 Goetz Gaycken
       * Fix coverity issues 14639,17116,14749,14771,14760,25175,14640-14642
         (uninitialised histogram pointer).

2014-11-18 Daniel Mori <dmori@cern.ch>
	* added defining parameter pull plots
	* TrkValHistUtils-00-00-19

2014-10-07 Felix Socher <Felix.Socher@cern.ch>
	* port to RootCore compatibility
	* removed TrkValHistUtilities
	* TrkValHistutils-00-00-18

2014-09-19 Daniel Mori <dmori@cern.ch>
	* fixed d0 and z0 plots
	* adjusted range for d0 and z0
	* added d0 plot with smaller range
	* TrkValHistUtils-00-00-17

2014-08-15 Ioannis Nomidis <ioannis.nomidis@cern.ch>
	* added BookTProfile for variable binning
	* TrkValHistUtils-00-00-16

2014-07-15 Felix Socher <felix.socher@cern.ch>
	* added pt_eta distribution to ParamPlots
	* TrkValHistUtils-00-00-14

2014-07-08 Felix Socher <felix.socher@cern.ch>
	* upon creation of a histogram the sumw2 method is called
	* deleted superfluous CreatedXDHist methods
	* TrkValHistUtils-00-00-14

2014-06-08 Felix Socher <felix.socher@cern.ch>
	* added more protections to TruthExtrapolationPlots
	* TrkValHistUtils-00-00-13

2014-06-03 Felix Socher <felix.socher@cern.ch>
	* added HitTypePlots
	* adapted MShitPlots, MSHitDiffPlots
	* TrkValHistUtils-00-00-12

2014-05-20 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* PlotBase to register TProfile (removed storing existing histograms)
	* TrkValHistUtils-00-00-11

2014-05-20 Simone Pagan Griso <simone.pagan.griso@cern.ch>
	* Added more xAOD::TrackParticle hits plots
	* Modified PlotBase to register new TH1 objects
	* TrkValHistUtils-00-00-10

2014-05-14 Niels van Eldik
	* adopt new naming convetions for links
	* TrkValHistUtils-00-00-09

2014-04-09 Felix Socher <felix.socher@cern.ch>
	* fixed small shadowing bug in PlotBase.h
	* TrkValHistUtils-00-00-08

2014-03-29 Shaun Roe
	* add a bool in plot base to turn on/off the directry path prepend
	* make strings in plot base to const reference
	* tag as TrkValHistUtils-00-00-07

2014-03-06  Felix Socher <felix.socher@cern.ch>
	* added TruthTkrExtrapolationPlots

2014-2-19 Shaun Roe
	* add fill methods in the IDHits

2014-2-14 Niels van Eldik
	* int -> uint8_t
	* tagging TrkValHistUtils-00-00-05

2014-02-12  Felix Socher  <felix.socher@cern.ch>
	* added HitDiffPlots
	* added utilities
	* altered PlotBase
	* tagging TrkValHistUtils-00-00-04

2014-02-12  Felix Socher  <felix.socher@cern.ch>
	* improved class naming
	* introduced namespace

2014-02-10  Felix Socher  <felix.socher@cern.ch>
	* added naming facilities to ParamPlots
	* added detail level to PlotBase

2014-02-10  Felix Socher  <felix.socher@cern.ch>
	* initial import of tracking related plot structures
