#ifndef INDETPHYSVALMONITORING_INDETDUMMYPLOTS
#define INDETPHYSVALMONITORING_INDETDUMMYPLOTS
/**
 * @file InDetDummyPlots.cxx
 *
 **/



// local includes
#include "InDetPlotBase.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTracking/TrackParticle.h"
// std includes
#include <string>

// fwd includes
class TH1;
class TProfile;
class TH2;

///class holding dummy plots
class InDetDummyPlots: public InDetPlotBase {
public:
  InDetDummyPlots(InDetPlotBase* pParent, const std::string& dirName);

  void algoEfficiency(double radius, int SiSPweight, int TRTSeededweight, int TRTStandaloneweight, int other_weight);
  void track_author(std::bitset<52> authorset);

private:
  TProfile* m_SiSPSeededFinder_eff_vs_radius;
  TProfile* m_TRTSeeded_eff_vs_radius;
  TProfile* m_TRTStandalone_eff_vs_radius;
  TProfile* m_combo_eff_vs_radius;
  TProfile* m_exclotherAlgo_eff_vs_radius;
  TProfile* m_otherAlgo_eff_vs_radius;

  TH1* m_track_author_use;

  // plot base has nop default implementation of this; we use it to book the histos
  void initializePlots();
  void finalizePlots();
};

#endif
